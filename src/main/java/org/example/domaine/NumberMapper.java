package org.example.domaine;

public class NumberMapper {

    public <T extends Number> PositiveNumber<T> numberToPositiveNumber(T number) {
        return new PositiveNumber<>(number);
    }

    public <T extends Number> T positiveNumberToNumber(PositiveNumber<T> positiveNumber) {
        return positiveNumber.getNumber();
    }
}
